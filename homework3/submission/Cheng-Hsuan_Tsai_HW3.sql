DROP PROCEDURE IF EXISTS `ShowRawScores`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ShowRawScores (IN ssn INT)
BEGIN
  -- sql queries go between BEGIN and END. End with ;'s
  DECLARE ssnMatch INT;

  SELECT COUNT(R.SSN)
  INTO ssnMatch
  FROM Rawscores as R
  WHERE R.SSN = ssn;

  IF ssnMatch > 0
  THEN
    SELECT *
    FROM Rawscores as R
    WHERE R.SSN = ssn;
  ELSE
    SELECT "SSN NOT FOUND!" as "msg";
  END IF;
-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
DROP PROCEDURE IF EXISTS `ShowPercentages`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ShowPercentages (IN ssn INT)
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE CumAvg DOUBLE;
  DECLARE Name varchar(20);
  DECLARE ssnMatch INT;

  SELECT COUNT(R.SSN)
  INTO ssnMatch
  FROM Rawscores as R
  WHERE R.SSN = ssn;

  IF ssnMatch > 0
  THEN
    SELECT R.SSN, R.LName, R.FName, R.Section,
      (R.HW1/total.HW1*100) as 'HW1%',
      (R.HW2a/total.HW2a*100) as 'HW2a%',
      (R.HW2b/total.HW2b*100) as 'HW2b%',
      (R.Midterm/total.Midterm*100) as 'Midterm%',
      (R.HW3/total.HW3*100) as 'HW3%',
      (R.FExam/total.FExam*100) as 'FExam%'
    FROM Rawscores as R, Rawscores as total
    WHERE R.SSN = ssn
      AND total.SSN = '0001';

    SELECT (
      (R.HW1/total.HW1*weight.HW1) +
      (R.HW2a/total.HW2a*weight.HW2a) +
      (R.HW2b/total.HW2b*weight.HW2b) +
      (R.Midterm/total.Midterm*weight.Midterm) +
      (R.HW3/total.HW3*weight.HW3) +
      (R.FExam/total.FExam*weight.FExam) )
    INTO CumAvg
    FROM Rawscores as R, Rawscores as total, Rawscores as weight
    WHERE R.SSN = ssn
      AND total.SSN = '0001'
      AND weight.SSN = '0002';

    SELECT CONCAT(R.FName, ' ', R.LName)
    INTO Name
    FROM Rawscores as R
    WHERE R.SSN = ssn;

    SELECT CONCAT('The cumulative course average for ', Name, ' is ', CumAvg) as "msg";
  ELSE
    SELECT "SSN NOT FOUND!" as 'msg';
  END IF;

-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
DROP PROCEDURE IF EXISTS `AllRawScores`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE AllRawScores (IN pwd varchar(20))
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(P.CurPasswords)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT *
    FROM Rawscores as R
    WHERE R.SSN != 0001 AND R.SSN != 0002
    ORDER BY R.SSN, R.LName, R.FName asc;
  ELSE
    SELECT "WRONG PASSWORD!";
  END IF;

-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
DROP PROCEDURE IF EXISTS `AllPercentages`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE AllPercentages (IN pwd varchar(20))
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(*)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT R.SSN, R.LName, R.FName, R.Section,
      (R.HW1/total.HW1*100) as 'HW1%',
      (R.HW2a/total.HW2a*100) as 'HW2a%',
      (R.HW2b/total.HW2b*100) as 'HW2b%',
      (R.Midterm/total.Midterm*100) as 'Midterm%',
      (R.HW3/total.HW3*100) as 'HW3%',
      (R.FExam/total.FExam*100) as 'FExam%',
      ((R.HW1/total.HW1*weight.HW1) +
      (R.HW2a/total.HW2a*weight.HW2a) +
      (R.HW2b/total.HW2b*weight.HW2b) +
      (R.Midterm/total.Midterm*weight.Midterm) +
      (R.HW3/total.HW3*weight.HW3) +
      (R.FExam/total.FExam*weight.FExam) ) as 'CumAvg'
    FROM Rawscores as R, Rawscores as total, Rawscores as weight
    WHERE total.SSN = '0001'
      AND weight.SSN = '0002'
      AND R.SSN != '0001'
      AND R.SSN != '0002'
    ORDER BY R.Section asc, CumAvg desc;
  ELSE
    SELECT "WRONG PASSWORD!";
  END IF;

-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
DROP PROCEDURE IF EXISTS `Stats`;
DROP PROCEDURE IF EXISTS `StatsPerSection`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

CREATE PROCEDURE StatsPerSection(IN section INT, IN pwd varchar(20))
BEGIN
  DECLARE matches INT;

  SELECT COUNT(*)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT 'Mean' as Statistic, temp.Section,
      AVG(temp.HW1) as HW1,
      AVG(temp.HW2a) as HW2a,
      AVG(temp.HW2b) as HW2b,
      AVG(temp.Midterm) as Midterm,
      AVG(temp.HW3) as HW3,
      AVG(temp.FExam) as FExam,
      AVG(temp.CumAvg) as CumAvg
    FROM (
      SELECT R.Section,
        (R.HW1/total.HW1*100) as HW1,
        (R.HW2a/total.HW2a*100) as HW2a,
        (R.HW2b/total.HW2b*100) as HW2b,
        (R.Midterm/total.Midterm*100) as Midterm,
        (R.HW3/total.HW3*100) as HW3,
        (R.FExam/total.FExam*100) as FExam,
        ((R.HW1/total.HW1*weight.HW1) +
        (R.HW2a/total.HW2a*weight.HW2a) +
        (R.HW2b/total.HW2b*weight.HW2b) +
        (R.Midterm/total.Midterm*weight.Midterm) +
        (R.HW3/total.HW3*weight.HW3) +
        (R.FExam/total.FExam*weight.FExam) ) as 'CumAvg'
      FROM Rawscores as R, Rawscores as total, Rawscores as weight
      WHERE total.SSN = '0001'
        AND weight.SSN = '0002'
        AND R.SSN != '0001'
        AND R.SSN != '0002'
        AND R.Section = section )as temp
    GROUP BY temp.Section;

    SELECT 'Minimum' as Statistic, temp.Section,
      MIN(temp.HW1) as HW1,
      MIN(temp.HW2a) as HW2a,
      MIN(temp.HW2b) as HW2b,
      MIN(temp.Midterm) as Midterm,
      MIN(temp.HW3) as HW3,
      MIN(temp.FExam) as FExam,
      MIN(temp.CumAvg) as CumAvg
    FROM (
      SELECT R.Section,
        (R.HW1/total.HW1*100) as HW1,
        (R.HW2a/total.HW2a*100) as HW2a,
        (R.HW2b/total.HW2b*100) as HW2b,
        (R.Midterm/total.Midterm*100) as Midterm,
        (R.HW3/total.HW3*100) as HW3,
        (R.FExam/total.FExam*100) as FExam,
        ((R.HW1/total.HW1*weight.HW1) +
        (R.HW2a/total.HW2a*weight.HW2a) +
        (R.HW2b/total.HW2b*weight.HW2b) +
        (R.Midterm/total.Midterm*weight.Midterm) +
        (R.HW3/total.HW3*weight.HW3) +
        (R.FExam/total.FExam*weight.FExam) ) as 'CumAvg'
      FROM Rawscores as R, Rawscores as total, Rawscores as weight
      WHERE total.SSN = '0001'
        AND weight.SSN = '0002'
        AND R.SSN != '0001'
        AND R.SSN != '0002'
        AND R.Section = section )as temp
    GROUP BY temp.Section;

    SELECT 'Maximum' as Statistic, temp.Section,
      MAX(temp.HW1) as HW1,
      MAX(temp.HW2a) as HW2a,
      MAX(temp.HW2b) as HW2b,
      MAX(temp.Midterm) as Midterm,
      MAX(temp.HW3) as HW3,
      MAX(temp.FExam) as FExam,
      MAX(temp.CumAvg) as CumAvg
    FROM (
      SELECT R.Section,
        (R.HW1/total.HW1*100) as HW1,
        (R.HW2a/total.HW2a*100) as HW2a,
        (R.HW2b/total.HW2b*100) as HW2b,
        (R.Midterm/total.Midterm*100) as Midterm,
        (R.HW3/total.HW3*100) as HW3,
        (R.FExam/total.FExam*100) as FExam,
        ((R.HW1/total.HW1*weight.HW1) +
        (R.HW2a/total.HW2a*weight.HW2a) +
        (R.HW2b/total.HW2b*weight.HW2b) +
        (R.Midterm/total.Midterm*weight.Midterm) +
        (R.HW3/total.HW3*weight.HW3) +
        (R.FExam/total.FExam*weight.FExam) ) as 'CumAvg'
      FROM Rawscores as R, Rawscores as total, Rawscores as weight
      WHERE total.SSN = '0001'
        AND weight.SSN = '0002'
        AND R.SSN != '0001'
        AND R.SSN != '0002'
        AND R.Section = section )as temp
    GROUP BY temp.Section;

    SELECT 'Std. Dev' as Statistic, temp.Section,
      STD(temp.HW1) as HW1,
      STD(temp.HW2a) as HW2a,
      STD(temp.HW2b) as HW2b,
      STD(temp.Midterm) as Midterm,
      STD(temp.HW3) as HW3,
      STD(temp.FExam) as FExam,
      STD(temp.CumAvg) as CumAvg
    FROM (
      SELECT R.Section,
        (R.HW1/total.HW1*100) as HW1,
        (R.HW2a/total.HW2a*100) as HW2a,
        (R.HW2b/total.HW2b*100) as HW2b,
        (R.Midterm/total.Midterm*100) as Midterm,
        (R.HW3/total.HW3*100) as HW3,
        (R.FExam/total.FExam*100) as FExam,
        ((R.HW1/total.HW1*weight.HW1) +
        (R.HW2a/total.HW2a*weight.HW2a) +
        (R.HW2b/total.HW2b*weight.HW2b) +
        (R.Midterm/total.Midterm*weight.Midterm) +
        (R.HW3/total.HW3*weight.HW3) +
        (R.FExam/total.FExam*weight.FExam) ) as 'CumAvg'
      FROM Rawscores as R, Rawscores as total, Rawscores as weight
      WHERE total.SSN = '0001'
        AND weight.SSN = '0002'
        AND R.SSN != '0001'
        AND R.SSN != '0002'
        AND R.Section = section )as temp
    GROUP BY temp.Section;
  END IF;
END#

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE Stats (IN pwd varchar(20))
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(*)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT R.SSN, R.LName, R.FName, R.Section,
      (R.HW1/total.HW1*100) as 'HW1%',
      (R.HW2a/total.HW2a*100) as 'HW2a%',
      (R.HW2b/total.HW2b*100) as 'HW2b%',
      (R.Midterm/total.Midterm*100) as 'Midterm%',
      (R.HW3/total.HW3*100) as 'HW3%',
      (R.FExam/total.FExam*100) as 'FExam%',
      ((R.HW1/total.HW1*weight.HW1) +
      (R.HW2a/total.HW2a*weight.HW2a) +
      (R.HW2b/total.HW2b*weight.HW2b) +
      (R.Midterm/total.Midterm*weight.Midterm) +
      (R.HW3/total.HW3*weight.HW3) +
      (R.FExam/total.FExam*weight.FExam) ) as 'CumAvg'
    FROM Rawscores as R, Rawscores as total, Rawscores as weight
    WHERE total.SSN = '0001'
      AND weight.SSN = '0002'
      AND R.SSN != '0001'
      AND R.SSN != '0002'
    ORDER BY R.Section asc, CumAvg desc;
    CALL StatsPerSection(315, pwd);
    CALL StatsPerSection(415, pwd);
 ELSE
    SELECT "WRONG PASSWORD!";
  END IF;

-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
DROP PROCEDURE IF EXISTS `ChangeScores`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #
-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ChangeScores (IN pwd varchar(20), IN ssn INT, IN AssignmentName varchar(20), IN newScore INT)
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(P.CurPasswords)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT *
    FROM Rawscores as R
    WHERE R.SSN = ssn;

    SET @query = CONCAT('UPDATE Rawscores SET ', AssignmentName, '=', newScore, ' WHERE SSN=', ssn);
    PREPARE stm1 FROM @query;
    EXECUTE stm1;
    DEALLOCATE PREPARE stm1;

    SELECT *
    FROM Rawscores as R
    WHERE R.SSN = ssn;
 ELSE
    SELECT "WRONG PASSWORD!";
  END IF;
-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
DROP PROCEDURE IF EXISTS `ChangeScoresForPHP`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #
-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ChangeScoresForPHP (IN pwd varchar(20), IN ssn INT, IN AssignmentName varchar(20), IN newScore INT)
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(P.CurPasswords)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SET @query = CONCAT('UPDATE Rawscores SET ', AssignmentName, '=', newScore, ' WHERE SSN=', ssn);
    PREPARE stm1 FROM @query;
    EXECUTE stm1;
    DEALLOCATE PREPARE stm1;
    SELECT "Update Successful!" AS msg;
 ELSE
    SELECT "Update Failed!" AS msg;
  END IF;
-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
