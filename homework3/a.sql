DROP PROCEDURE IF EXISTS `ShowRawScores`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ShowRawScores (IN ssn INT)
BEGIN
  -- sql queries go between BEGIN and END. End with ;'s
  DECLARE ssnMatch INT;

  SELECT COUNT(R.SSN)
  INTO ssnMatch
  FROM Rawscores as R
  WHERE R.SSN = ssn;

  IF ssnMatch > 0
  THEN
    SELECT *
    FROM Rawscores as R
    WHERE R.SSN = ssn;
  ELSE
    SELECT "SSN NOT FOUND!" as "msg";
  END IF;
-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
