DROP PROCEDURE IF EXISTS `ChangeScores`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #
-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ChangeScores (IN pwd varchar(20), IN ssn INT, IN AssignmentName varchar(20), IN newScore INT)
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(P.CurPasswords)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT *
    FROM Rawscores as R
    WHERE R.SSN = ssn;

    SET @query = CONCAT('UPDATE Rawscores SET ', AssignmentName, '=', newScore, ' WHERE SSN=', ssn);
    PREPARE stm1 FROM @query;
    EXECUTE stm1;
    DEALLOCATE PREPARE stm1;

    SELECT *
    FROM Rawscores as R
    WHERE R.SSN = ssn;
 ELSE
    SELECT "WRONG PASSWORD!";
  END IF;
-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
