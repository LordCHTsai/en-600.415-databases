DROP PROCEDURE IF EXISTS `AllPercentages`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE AllPercentages (IN pwd varchar(20))
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(*)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT R.SSN, R.LName, R.FName, R.Section,
      (R.HW1/total.HW1*100) as 'HW1%',
      (R.HW2a/total.HW2a*100) as 'HW2a%',
      (R.HW2b/total.HW2b*100) as 'HW2b%',
      (R.Midterm/total.Midterm*100) as 'Midterm%',
      (R.HW3/total.HW3*100) as 'HW3%',
      (R.FExam/total.FExam*100) as 'FExam%',
      ((R.HW1/total.HW1*weight.HW1) +
      (R.HW2a/total.HW2a*weight.HW2a) +
      (R.HW2b/total.HW2b*weight.HW2b) +
      (R.Midterm/total.Midterm*weight.Midterm) +
      (R.HW3/total.HW3*weight.HW3) +
      (R.FExam/total.FExam*weight.FExam) ) as 'CumAvg'
    FROM Rawscores as R, Rawscores as total, Rawscores as weight
    WHERE total.SSN = '0001'
      AND weight.SSN = '0002'
      AND R.SSN != '0001'
      AND R.SSN != '0002'
    ORDER BY R.Section asc, CumAvg desc;
  ELSE
    SELECT "WRONG PASSWORD!";
  END IF;

-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
