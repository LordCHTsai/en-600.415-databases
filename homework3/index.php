<?php
  $action = $_GET['Action'];
  $con = mysqli_connect("dbase.cs.jhu.edu", "cs41513_ctsai20", "4wBMpiCF00", "cs41513_ctsai20_db");
  if (mysqli_connect_errno($con))
  {
    echo "Failed to connect to MySQL:" . mysqli_connect_error();
  }
  else
  {
    switch ($action)
    {
      case "Show":
        $query = "SELECT * FROM Rawscores as R WHERE R.SSN != '0001' AND R.SSN != '0002' ORDER BY R.Section asc;";
        $r = array();
        $result = mysqli_query($con, $query);
        while($row = mysqli_fetch_object($result))
        {
          array_push($r, $row);
        }
        echo json_encode($r);
        mysqli_close($con);
        exit();
        break;
      case "Update":
        $ssn = $_GET['SSN'];
        $which = $_GET['which'];
        $score = $_GET['score'];
        if ( $ssn && $which && $score )
        {
          $query = "UPDATE Rawscores SET ".$which." = ".$score." WHERE SSN=".$ssn;
          $result = mysqli_query($con, $query);
          mysqli_close($con);
          exit();
        }
        break;
      default:
        break;
    }
?>
<html ng-app="database">

  <head>
    <style type="text/css">
    body {
      text-align: center;
      display: block;
      height: 100%;
      width: 100%;
      padding: 0px;
      background-color: rgba(150,255,150,0.2);
    }
    .container {
      text-align: left;
      width: 70%;
      //height: 100%;
      background-color: #C8FFBF;
      margin-left: auto;
      margin-right: auto;
      padding-bottom: 10px;
      padding-top: 10px;
      border: dashed rgba(50,50,50,0.5);
      -webkit-border-radius: 10px;
      -moz-border-radius: 10px;
      -ms-border-radius: 10px;
      -o-border-radius: 10px;
      border-radius: 10px;
    }
    .gridStyle {
      border: 1px solid rgb(212,212,212);
      width: 800px;
      height: 400px;
      margin: auto;
    }
    .grid {
      width: 100%;
      text-align: center;
    }
    .refreshBtn
    {
      width: 800px;
      margin: auto;
      text-align: right;
    }
    .update {
      width: 800px;
      margin: auto;
      text-align: left;
      background-color: rgba(100,100,100,0.2);
    }
    </style>
    <link rel="stylesheet" type="text/css" href="http://angular-ui.github.com/ng-grid/css/ng-grid.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.1/angular.min.js"></script>
    <script type="text/javascript" src="http://angular-ui.github.com/ng-grid/lib/ng-grid.debug.js"></script>
    <script>
      angular.module('database', ['ngGrid'])
      .controller('mainCtrl', function($scope, $http){
        $scope.score = 0;
        $scope.renewTable = function(ssn){
          var payload = 'Action=Show';
          if (angular.isDefined(ssn)) payload = payload + "&SSN=" + ssn;
          $http.get('./index.php?' + payload)
          .success(function(data){
            $scope.table = data;
            $scope.list = angular.fromJson(data);
          });
        };
        $scope.update = function(ssn, which, score){
          var payload = 'Action=Update';
          if (angular.isDefined(ssn) && angular.isDefined(which) && angular.isDefined(score))
          {
            payload = payload + "&SSN=" + ssn + "&which=" + which + "&score=" + score;

            $http.get('./index.php?' + payload)
            .success(function(data){
              $scope.renewTable();
            });
          }
        };

        $scope.gridOptions = { data: 'table'};
        $scope.col = [{ name: 'SSN'},
                      { name: 'LName'},
                      { name: 'FName'},
                      { name: 'Section'},
                      { name: 'HW1'},
                      { name: 'HW2a'},
                      { name: 'HW2b'},
                      { name: 'Midterm'},
                      { name: 'HW3'},
                      { name: 'FExam'}];
        $scope.which = [{ name: 'HW1'},
                        { name: 'HW2a'},
                        { name: 'HW2b'},
                        { name: 'Midterm'},
                        { name: 'HW3'},
                        { name: 'FExam'}];
      });
    </script>
  </head>

  <body ng-controller="mainCtrl" ng-init="renewTable()">
    <div class="container">
      <div class="update">
        <div>UPDATE score:</div>
        <div>
          <div>
          SSN:<select ng-model="ssn" ng-options="row.SSN for row in list"></select>
          {{ssn['FName']}} {{ssn['LName']}}
          </div>
          <div>
            Score:
            <select ng-model="whichOne" ng-options="r.name for r in which"></select>
            {{ssn[whichOne.name]}}
          </div>
          <div>
            New Score:<input type="number" ng-model="score">{{score}}
          </div>
        <button ng-click="update(ssn.SSN, whichOne.name, score)">Submit</button>
        </div>
      </div>
      <div class="grid">
        <div class="refreshBtn"><button ng-click="renewTable()">Refresh</button></div>
        <div class="gridStyle" ng-grid="gridOptions"></div>
      </div>
    </div>
  </body>

</html>
<?php
  mysqli_close($con);
  }
?>
