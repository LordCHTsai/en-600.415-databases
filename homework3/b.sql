DROP PROCEDURE IF EXISTS `ShowPercentages`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ShowPercentages (IN ssn INT)
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE CumAvg DOUBLE;
  DECLARE Name varchar(20);
  DECLARE ssnMatch INT;

  SELECT COUNT(R.SSN)
  INTO ssnMatch
  FROM Rawscores as R
  WHERE R.SSN = ssn;

  IF ssnMatch > 0
  THEN
    SELECT R.SSN, R.LName, R.FName, R.Section,
      (R.HW1/total.HW1*100) as 'HW1%',
      (R.HW2a/total.HW2a*100) as 'HW2a%',
      (R.HW2b/total.HW2b*100) as 'HW2b%',
      (R.Midterm/total.Midterm*100) as 'Midterm%',
      (R.HW3/total.HW3*100) as 'HW3%',
      (R.FExam/total.FExam*100) as 'FExam%'
    FROM Rawscores as R, Rawscores as total
    WHERE R.SSN = ssn
      AND total.SSN = '0001';

    SELECT (
      (R.HW1/total.HW1*weight.HW1) +
      (R.HW2a/total.HW2a*weight.HW2a) +
      (R.HW2b/total.HW2b*weight.HW2b) +
      (R.Midterm/total.Midterm*weight.Midterm) +
      (R.HW3/total.HW3*weight.HW3) +
      (R.FExam/total.FExam*weight.FExam) )
    INTO CumAvg
    FROM Rawscores as R, Rawscores as total, Rawscores as weight
    WHERE R.SSN = ssn
      AND total.SSN = '0001'
      AND weight.SSN = '0002';

    SELECT CONCAT(R.FName, ' ', R.LName)
    INTO Name
    FROM Rawscores as R
    WHERE R.SSN = ssn;

    SELECT CONCAT('The cumulative course average for ', Name, ' is ', CumAvg) as "msg";
  ELSE
    SELECT "SSN NOT FOUND!" as 'msg';
  END IF;

-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
