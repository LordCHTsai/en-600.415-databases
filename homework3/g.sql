DROP PROCEDURE IF EXISTS `ChangeScoresForPHP`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #
-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE ChangeScoresForPHP (IN pwd varchar(20), IN ssn INT, IN AssignmentName varchar(20), IN newScore INT)
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(P.CurPasswords)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SET @query = CONCAT('UPDATE Rawscores SET ', AssignmentName, '=', newScore, ' WHERE SSN=', ssn);
    PREPARE stm1 FROM @query;
    EXECUTE stm1;
    DEALLOCATE PREPARE stm1;
    SELECT "Update Successful!" AS msg;
 ELSE
    SELECT "Update Failed!" AS msg;
  END IF;
-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
