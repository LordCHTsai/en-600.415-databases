DROP PROCEDURE IF EXISTS `AllRawScores`;
-- change delimiter! or else ;'s will cause mysql to stop parsing
delimiter #

-- define procedure and parameters (no parameters in this case)
CREATE PROCEDURE AllRawScores (IN pwd varchar(20))
BEGIN
-- sql queries go between BEGIN and END. End with ;'s
  DECLARE matches INT;

  SELECT COUNT(P.CurPasswords)
  INTO matches
  FROM Passwords as P
  WHERE P.CurPasswords = pwd;

  IF matches > 0
  THEN
    SELECT *
    FROM Rawscores as R
    WHERE R.SSN != 0001 AND R.SSN != 0002
    ORDER BY R.SSN, R.LName, R.FName asc;
  ELSE
    SELECT "WRONG PASSWORD!";
  END IF;

-- use the delimiter to indicate end of create statement
END#

-- revert changes to delimeter
delimiter ;
