#7
select S1.Fname, S1.LName, S1.Age
from Student as S1
	join
		(
			select temp.Major, temp.Age as minAge
			from
				(
					select *
					from Student
					order by Major asc, Age asc
				) as temp
			group by Major
		) as temp2 on S1.Major = temp2.Major
where S1.Age = temp2.minAge;