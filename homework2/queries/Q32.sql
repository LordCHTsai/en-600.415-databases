#32
select concat(S1.Fname, ' ', S1.LName) as Name, S1.Age, S1.Major
from
	(
		select S1.StuID, avg(G1.gradepoint) as major_GPA
		from Student as S1, Enrolled_in as E1, Gradeconversion as G1
		where
			S1.StuID = E1.StuID and
			E1.CID like concat(S1.Major,"%") and
			G1.lettergrade = E1.Grade
		group by S1.StuID
	) as temp1,
	(
		select S1.StuID, avg(G1.gradepoint) as non_major_GPA
		from Student as S1, Enrolled_in as E1, Gradeconversion as G1
		where
			S1.StuID = E1.StuID and
			E1.CID not like concat(S1.Major,"%") and
			G1.lettergrade = E1.Grade
		group by S1.StuID
	) as temp2,
	Student as S1
where
	temp1.StuID = temp2.StuID and
	temp2.non_major_GPA > temp1.major_GPA and
	S1.StuID = temp1.StuID;