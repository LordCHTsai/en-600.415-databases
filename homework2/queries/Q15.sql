#15
select *
from Lives_in as LI
	left join Lives_in as LI2 on LI.dormid = LI2.dormid and LI.room_number = LI2.room_number
	left join Student as S1 on S1.StuID = LI.stuid
group by LI.room_number
having count(LI.room_number) = 1;