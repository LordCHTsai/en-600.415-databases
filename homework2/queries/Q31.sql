#31
select LI1.dormid, avg(DA1.distance) as avgDistance
from Lives_in as LI1, Student as S1, Direct_distance as DA1
where
	LI1.stuid = S1.StuID and
	DA1.city2_code = S1.city_code and
	DA1.city1_code = 'BAL'
group by LI1.dormid
having avg(DA1.distance) >= 
	(
		select max(temp1.avgDistance)
		from
			(
				select LI1.dormid, avg(DA1.distance) as avgDistance
				from Lives_in as LI1, Student as S1, Direct_distance as DA1
				where
					LI1.stuid = S1.StuID and
					DA1.city2_code = S1.city_code and
					DA1.city1_code = 'BAL'
				group by LI1.dormid
			) as temp1
	);