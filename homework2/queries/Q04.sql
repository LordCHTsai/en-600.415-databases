#4
select C1.CName, S1.Major as S1_Major, E1.Grade as S1_Grade, concat(S1.Fname, " ",S1.LName) as S1_name,
				S2.Major as S2_Major, E2.Grade as S2_Grade, concat(S2.Fname, " ",S2.LName) as S2_name
from Loves as Lv1
	left join Enrolled_in as E1 on Lv1.WhoLoves = E1.StuID
	left join Student as S1 on S1.StuID = Lv1.WhoLoves
	left join Loves as Lv2 on Lv1.WhoLoves = Lv2.WhoIsLoved and Lv1.WhoIsLoved = Lv2.WhoLoves
	left join Enrolled_in as E2 on Lv2.WhoLoves = E2.StuID
	left join Student as S2 on S2.StuID = Lv2.WhoLoves
	left join Course as C1 on C1.CID = E1.CID
where E1.CID = E2.CID;