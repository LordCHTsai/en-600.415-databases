#39
select distinct concat(F1.Fname, ' ', F1.LName) as Faculty_Name
from Student as S1
	left join Member_of as M1 on S1.Advisor = M1.FacID
	left join Enrolled_in as E1 on E1.StuID = S1.StuID
	left join Enrolled_in as E2 on E1.CID = E2.CID and E1.StuID != E2.StuID
	left join Lives_in as LI1 on LI1.stuid = E2.StuID
	left join Lives_in as LI2 on LI1.dormid = LI2.dormid and LI1.room_number = LI2.room_number and LI1.stuid != LI2.stuid
	left join Student as S2 on LI2.stuid = S2.StuID
	left join Minor_in as MI1 on S2.StuID = MI1.StuID
	left join Faculty as F1 on F1.FacID = S1.Advisor 
where
	S2.Major = M1.DNO or
	MI1.DNO = M1.DNO;