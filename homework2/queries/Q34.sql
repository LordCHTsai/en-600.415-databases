#34
select A1.activity_name, count(distinct P1.stuid) as student_members
from Activity as A1, Participates_in as P1, Faculty_Participates_in as F1
where
	A1.actid = P1.actid and
	A1.actid = F1.actid
group by A1.actid
having count(distinct P1.stuid)+count(distinct F1.FacID) >=
	(
		select max(temp1.total_members)
		from
			(
				select count(distinct P1.stuid)+count(distinct F1.FacID) as total_members
				from Activity as A1, Participates_in as P1, Faculty_Participates_in as F1
				where
					A1.actid = P1.actid and
					A1.actid = F1.actid
				group by A1.actid
			) as temp1
	);