#10
select D2.DName
from Student as S1
	left join Department as D1 on S1.Major = D1.DNO
	inner join Minor_in as M1 on M1.StuID = S1.StuID
	left join Department as D2 on M1.DNO = D2.DNO
	where D1.DName like 'math%' or D1.DName like 'computer%'
	group by M1.DNO
	having count(M1.DNO) = 
		(
			select max(temp.total)
			from
				(
					select M1.DNO, count(M1.DNO) as total
					from Student as S1
						left join Department as D1 on S1.Major = D1.DNO
						inner join Minor_in as M1 on M1.StuID = S1.StuID
					where D1.DName like 'math%' or D1.DName like 'computer%'
					group by M1.DNO
				) as temp
		);