#25
select E1.CID, count(E1.Grade) as Above_A_minus, temp1.total_enroll, (count(E1.Grade)/temp1.total_enroll*100) as percentage
from Enrolled_in as E1
	left join
		(
			select E2.CID, count(E2.StuID) as total_enroll
			from Enrolled_in as E2
			group by E2.CID
		) as temp1 on temp1.CID = E1.CID
where
	E1.Grade in ('A', 'A-', 'A+') and
	temp1.total_enroll > 0
group by E1.CID;