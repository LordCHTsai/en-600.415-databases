#38
select S1.Major, count(P1.actid)/count(distinct S1.StuID) as average_activities
from Student as S1, Participates_in as P1
where
	S1.StuID = P1.stuid
group by Major;