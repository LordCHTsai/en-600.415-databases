#35
select temp1.activity_name
from
	(
		select A1.*
		from Activity as A1
			left join Participates_in as P1 on A1.actid = P1.actid
		group by A1.actid
		having count(P1.stuid) = 0
	) as temp1
	left join Faculty_Participates_in as F1 on temp1.actid = F1.actid
group by temp1.actid
having count(F1.FacID) > 0;