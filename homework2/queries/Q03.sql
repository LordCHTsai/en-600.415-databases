#3
select S1.StuID, S1.LName, S1.Fname, S1.Major, S1.Sex, E1.Grade
from Student as S1
	left join Likes as L1 on S1.StuID = L1.WhoLikes
	left join Enrolled_in as E1 on L1.WhoIsLiked = E1.StuID
	left join Course as C1 on C1.CID = E1.CID
where C1.CName like 'algorithm%'
group by S1.StuID
having count(distinct L1.WhoIsLiked) = 
	(
		select count(distinct E1.StuID)
		from Enrolled_in as E1
		left join Course as C1 on E1.CID = C1.CID
		where C1.CName like 'algorithm%'
	);