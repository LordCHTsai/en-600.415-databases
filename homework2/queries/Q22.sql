#22
select concat(S1.Fname, ' ', S1.LName) as Name, S1.Age
from Student as S1, Minor_in as M1, Department as D1, Member_of as Me1, Faculty as F1
where
	S1.StuID = M1.StuID and
	M1.DNO = D1.DNO and
	D1.DNO = Me1.DNO and
	F1.FacID = Me1.FacID and
	F1.Sex = 'F' and
	Me1.Appt_Type like '%primary%' and
	D1.Division = 'EN';