#24
select concat(S1.Fname, ' ', S1.LName) as Name, S1.StuID
from Student as S1, Enrolled_in as E1, Enrolled_in as E2, Enrolled_in as E3, Student as S2
where
	S1.StuID = E1.StuID and
	E1.CID = E2.CID and
	E1.StuID != E2.StuID and
	E2.CID = E3.CID and
	E2.StuID != E3.StuID and
	E3.StuID = S2.StuID and
	E1.StuID != E3.StuID and
	S2.Fname = 'Linda' and
	S2.LName = 'Smith';