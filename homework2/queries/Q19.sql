#19
select concat(F1.Fname, ' ', F1.Lname) as F_name, count(E1.StuID) as total_enrollment
from Course as C1
	left join Faculty as F1 on C1.Instructor = F1.FacID
	left join Member_of as M1 on M1.FacID = F1.FacID
	left join Department as D1 on D1.DNO = C1.DNO
	inner join Enrolled_in as E1 on E1.CID = C1.CID
where D1.DName like 'computer%' and M1.Appt_Type like 'primary%'
group by F1.FacID
order by total_enrollment desc;