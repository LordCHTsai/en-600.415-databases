#14
select temp1.Fname, temp1.LName, temp2.LikesNum, count(distinct Lv2.WhoLikes) as LikedNum, temp2.LikesNum - count(distinct Lv2.WhoLikes) as Difference
from Likes as Lv1
	left join
		(
			select S1.StuID, S1.Fname, S1.LName
			from Student as S1
		) as temp1 on temp1.StuID = Lv1.WhoLikes
	left join
		(
			select Lv2.WhoLikes as ID, count(Lv2.WhoIsLiked) as LikesNum
			from Likes as Lv2
			group by Lv2.WhoLikes
		) as temp2 on temp2.ID = Lv1.WhoLikes
	left join Likes as Lv2 on Lv2.WhoIsLiked = Lv1.WhoLikes
where Lv1.WhoLikes = Lv2.WhoIsLiked
group by Lv2.WhoIsLiked;