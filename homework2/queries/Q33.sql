#33
select D1.DName, count(distinct S1.StuID)/count(distinct M1.FacID) as mean_number_of_advisee
from Member_of as M1, Department as D1, Student as S1
where
	D1.DNO = M1.DNO and
	M1.Appt_Type like '%primary%' and
	S1.Advisor = M1.FacID
group by D1.DNO;