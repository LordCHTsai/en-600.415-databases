#16
select *
from Student as S1
where S1.StuID not in
	(
		select S2.StuID
		from Student as S2
		inner join Lives_in as LI1 on S2.StuID = LI1.stuid
	);