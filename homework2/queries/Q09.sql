#9
select distinct D1.DName, F1.Fname, F1.Lname
from Student as S1
	left join Enrolled_in as E1 on S1.StuID = E1.StuID
	left join
		(
			select E1.CID ,avg(S1.Age) as AvgAge
			from Enrolled_in as E1
				left join Student as S1 on E1.StuID = S1.StuID
			group by E1.CID
		) as temp1 on E1.CID = temp1.CID
	left join Faculty as F1 on F1.FacID = S1.Advisor
	left join Department as D1 on D1.DNO = S1.Major
where S1.Age < AvgAge;