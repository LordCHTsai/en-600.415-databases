#6
select S1.FName, S1.Lname, S2.FName, S2.Lname, S3.FName, S3.Lname 
from Loves as Lv1
	left join (select StuID, FName, Lname from Student) as S1 on Lv1.WhoLoves = S1.StuID
	join Loves as Lv2 on Lv1.WhoIsLoved = Lv2.WhoLoves
	left join (select StuID, FName, Lname from Student) as S2 on Lv2.WhoLoves = S2.StuID
	join Loves as Lv3 on Lv2.WhoIsLoved = Lv3.WhoLoves
	left join (select StuID, FName, Lname from Student) as S3 on Lv3.WhoLoves = S3.StuID
where Lv2.WhoIsLoved = Lv3.WhoLoves and Lv3.WhoIsLoved = Lv1.WhoLoves;