#26
select temp1.CName, temp1.CID, concat(F1.Fname, ' ', F1.Lname) as Name
from
	(
		select F1.FacID, C1.CID, C1.CName, (count(E1.Grade)/temp1.total_enroll*100) as percentage
		from Enrolled_in as E1
			left join
				(
					select E2.CID, count(E2.StuID) as total_enroll
					from Enrolled_in as E2
					group by E2.CID
				) as temp1 on temp1.CID = E1.CID
			left join Gradeconversion as G1 on E1.Grade = G1.lettergrade
			left join Course as C1 on C1.CID = E1.CID
			left join Faculty as F1 on F1.FacID = C1.Instructor
		where
			G1.gradepoint < 2.7 and
			temp1.total_enroll > 0
		group by E1.CID
	) as temp1
	left join Faculty as F1 on F1.FacID = temp1.FacID
where temp1.percentage = 
	(
		select max(temp2.percentage)
		from
			(
				select (count(E1.Grade)/temp1.total_enroll*100) as percentage
				from Enrolled_in as E1
					left join
						(
							select E2.CID, count(E2.StuID) as total_enroll
							from Enrolled_in as E2
							group by E2.CID
						) as temp1 on temp1.CID = E1.CID
					left join Gradeconversion as G1 on E1.Grade = G1.lettergrade
					left join Course as C1 on C1.CID = E1.CID
					left join Faculty as F1 on F1.FacID = C1.Instructor
				where
					G1.gradepoint < 2.7 and
					temp1.total_enroll > 0
				group by E1.CID
			) as temp2
	);