#13
select S1.StuID, S1.Fname, S1.LName, C1.CID, C1.CName
from Loves as Lv1
	left join Enrolled_in as E1 on E1.StuID = Lv1.WhoIsLoved
	left join Course as C1 on E1.CID = C1.CID
	left join Student as S1 on S1.StuID = Lv1.WhoLoves;