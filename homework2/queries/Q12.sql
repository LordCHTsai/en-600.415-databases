#12
select distinct temp1.StuID, temp1.Fname, temp1.LName, temp1.Major, temp1.GPA, temp2.CID, C1.CName, temp2.Grade
from
	(
		select S1.*, (sum(gradepoint * Credits)/sum(Credits)) as GPA
		from Student as S1
			left join Enrolled_in as E1 on S1.StuID = E1.StuID
			left join Course as C1 on C1.CID = E1.CID
			left join Department as D1 on S1.Major = D1.DNO
			left join Gradeconversion as G1 on E1.Grade = G1.lettergrade
		group by S1.StuID
	) as temp1
	inner join
	(
		select *
		from Enrolled_in
		where Grade = 'A' or Grade = 'A+'
	) as temp2 on temp1.StuID = temp2.StuID 
	left join	Course as C1 on C1.CID = temp2.CID
where temp1.GPA >=
	(
		select gradepoint
		from Gradeconversion
		where lettergrade = 'B+'
	);