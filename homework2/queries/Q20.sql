#20
select *
from Student as S1
	left join Enrolled_in as E1 on E1.StuID = S1.StuID
	left join Course as C1 on C1.CID = E1.CID
	left join
		(
			select E1.StuID, sum(C1.Credits) total_credits
			from Enrolled_in as E1
				left join Course as C1 on E1.CID = C1.CID
			group by E1.StuID
		) as temp1 on temp1.StuID = S1.StuID
where C1.CName like '%database%' and
	temp1.total_credits > 
		(
			select avg(C1.Credits)
			from Enrolled_in as E1
				inner join Course as C1 on C1.CID = E1.CID
		);