#5
select S1.LName, count(S1.LName)
from Student as S1
where S1.Sex = 'F'
group by S1.LName
having count(S1.LName) = 
	(
		select max(temp.total)
		from
			(
				select count(S2.LName) as total
				from Student as S2
				where S2.Sex = 'F'
				group by S2.LName
			) as temp
	);