#40
select concat(S1.Fname, ' ', S1.LName) as Name, S1.Major, sum(C1.Credits) as total_credit, temp1.average_credits
from Student as S1
	left join Enrolled_in as E1 on S1.StuID = E1.StuID
	left join Course as C1 on E1.CID = C1.CID
	left join
		(
			select S1.Major, sum(C1.Credits)/count(distinct S1.StuID) as average_credits
			from Student as S1
				left join Enrolled_in as E1 on S1.StuID = E1.StuID
				left join Course as C1 on E1.CID = C1.CID
			group by S1.Major
		) as temp1 on S1.Major = temp1.Major
group by S1.StuID
having sum(C1.Credits) > temp1.average_credits;