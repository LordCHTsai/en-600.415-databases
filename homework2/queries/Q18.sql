#18
select S1.LName, S1.Fname, S1.Age, S1.Sex
from Student as S1
	left join Enrolled_in as E1 on E1.StuID = S1.StuID
where E1.CID = '600.315'
	or E1.CID = '600.415'
	or E1.CID like '600.1%'
	or E1.CID like '600.2%';