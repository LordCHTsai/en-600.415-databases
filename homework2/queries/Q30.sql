#30-a
select distinct D1.city2_code as city1, D2.city2_code as city2, D1.distance + D2.distance as distance_through_BAL
from Direct_distance as D1, Direct_distance as D2
where
	D1.city1_code = 'BAL' and
	D1.city1_code = D2.city1_code and D1.city2_code != D2.city2_code and D1.city1_code != D2.city2_code;

#30-b
select distinct C1.city_code as city1, C2.city_code as city2, 
	sqrt(power((70*(C1.latitude - C2.latitude)),2)+power((70*(C1.longitude - C2.longitude)),2)) as rec_distance
from City as C1, City as C2
where
	C1.city_code != C2.city_code;
	
#30-c
select distinct temp1.city1, temp1.city2, D1.distance, temp1.rec_distance, temp2.distance_through_BAL
from 
	(
		select distinct C1.city_code as city1, C2.city_code as city2, 
			sqrt(power((70*(C1.latitude - C2.latitude)),2)+power((70*(C1.longitude - C2.longitude)),2)) as rec_distance
		from City as C1, City as C2
		where
			C1.city_code != C2.city_code
	) as temp1
	left join
	(
		select distinct D1.city2_code as city1, D2.city2_code as city2, D1.distance + D2.distance as distance_through_BAL
		from Direct_distance as D1, Direct_distance as D2
		where
			D1.city1_code = 'BAL' and
			D1.city1_code = D2.city1_code and D1.city2_code != D2.city2_code and D1.city1_code != D2.city2_code
	) as temp2 on
		temp1.city1 = temp2.city1 and temp1.city2 = temp2.city2 or
		temp1.city2 = temp2.city1 and temp1.city1 = temp2.city2
	left join Direct_distance as D1 on
		D1.city1_code = temp1.city1 and D1.city2_code = temp1.city2 or
		D1.city2_code = temp1.city1 and D1.city1_code = temp1.city2;

#30-d
select *
from
(
	(
		select distinct D1.city2_code as city1, D2.city2_code as city2, D1.distance + D2.distance as distance
		from Direct_distance as D1, Direct_distance as D2
		where
			D1.city1_code = 'BAL' and
			D1.city1_code = D2.city1_code and D1.city2_code != D2.city2_code and D1.city1_code != D2.city2_code
	)
	union
	(
		select distinct C1.city_code as city1, C2.city_code as city2, 
			sqrt(power((70*(C1.latitude - C2.latitude)),2)+power((70*(C1.longitude - C2.longitude)),2)) as distance
		from City as C1, City as C2
		where
			C1.city_code != C2.city_code
	)
	union
	(
		select distinct D1.city1_code as city1, D1.city2_code as city2, D1.distance
		from Direct_distance as D1
	)
) as temp
group by temp.city1, temp.city2
order by temp.distance asc;