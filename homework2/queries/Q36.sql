#36
select distinct concat(S1.Fname, ' ', S1.LName) as Name
from Enrolled_in as E1, Student as S1, Enrolled_in as E2, Lives_in as LI1, Lives_in as LI2, Student as S2, City as C1
where
	E1.StuID = S1.StuID and
	E1.CID = E2.CID and
	E1.StuID != E2.StuID and
 	E2.StuID = LI1.stuid and
	LI1.dormid = LI2.dormid and
	LI1.room_number = LI2.room_number and
	LI1.stuid != LI2.stuid and
	LI2.stuid = S2.StuID and
	S2.city_code = C1.city_code and
	C1.state = 'PA';
	