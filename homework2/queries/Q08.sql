#8
select S1.Fname, count(S1.Fname)
from Student as S1
where S1.Sex = 'F'
group by S1.Fname
having count(S1.Fname) = 
	(
		select max(temp.total)
		from
			(
				select count(S2.Fname) as total
				from Student as S2
				where S2.Sex = 'F'
				group by S2.Fname
			) as temp
	);