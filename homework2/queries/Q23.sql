#23
select S1.StuID, concat(S1.Fname, ' ', S1.LName) as Name
from Student as S1, Enrolled_in as E1, Course as C1, Faculty as F1
where
	S1.StuID = E1.StuID and
	E1.CID = C1.CID and
	C1.Instructor = F1.FacID and
	F1.Fname = 'Paul' and
	F1.Lname = 'Smolensky';