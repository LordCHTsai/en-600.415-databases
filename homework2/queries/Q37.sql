#37
select distinct concat(S1.Fname,' ', S1.LName) as Name1, concat(S2.Fname,' ', S2.LName) as Name2
from Lives_in as LI1, Lives_in as LI2, Student as S1, Student as S2, City as C1, City as C2
where
	LI1.dormid = LI2.dormid and
	LI1.room_number = LI2.room_number and
	LI1.stuid < LI2.stuid and ##!!important: prevent duplicated pairs
	S1.StuID = LI1.stuid and
	S2.StuID = LI2.stuid and
	S1.city_code = C1.city_code and
	S2.city_code = C2.city_code and
	C1.country != C2.country;