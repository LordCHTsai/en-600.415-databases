#2
select DName, GPA, Fname, LName
from
	(
		select Student.StuID, LName, Fname, Major, DName, (sum(gradepoint * Credits)/sum(Credits)) as GPA
		from Student
			left join Enrolled_in on Student.StuID = Enrolled_in.StuID
			left join Course on Course.CID = Enrolled_in.CID
			left join Department on Student.Major = Department.DNO
			left join Gradeconversion on Enrolled_in.Grade = Gradeconversion.lettergrade
		group by Student.StuID
		order by Major, GPA asc
	) as temp
group by Major;