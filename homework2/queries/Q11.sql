#11
select distinct * 
from Preferences as P1
	left join (select StuID, LName, Fname from Student) as S1 on P1.StuID = S1.StuID
	left join Preferences as P2 on P1.StuID != P2.StuID
	left join (select StuID, LName, Fname from Student) as S2 on P2.StuID = S2.StuID
where P1.SleepHabits != P2.SleepHabits
	or P1.MusicType != P2.MusicType
	or P1.Smoking != P2.Smoking;