#21
select *
from Student as S1
where S1.Age >
	(
		select std(Age)+avg(Age)
		from Student as S1
		left join Enrolled_in as E1 on S1.StuID = E1.StuID
		left join Course as C1 on C1.CID = E1.CID
		where C1.CName like '%database%'
	);