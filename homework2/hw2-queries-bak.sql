#1
select Fname, Lname, Age
from Student
where Age = (
    select min(Age)
    from Student
);

#2
select DName, GPA, Fname, LName
from
    (
        select Student.StuID, LName, Fname, Major, DName, (sum(gradepoint * Credits)/sum(Credits)) as GPA
        from Student
            left join Enrolled_in on Student.StuID = Enrolled_in.StuID
            left join Course on Course.CID = Enrolled_in.CID
            left join Department on Student.Major = Department.DNO
            left join Gradeconversion on Enrolled_in.Grade = Gradeconversion.lettergrade
        group by Student.StuID
        order by Major, GPA asc
    ) as temp
group by Major;

#3
select S1.StuID, S1.LName, S1.Fname, S1.Major, S1.Sex, E1.Grade
from Student as S1
    left join Likes as L1 on S1.StuID = L1.WhoLikes
    left join Enrolled_in as E1 on L1.WhoIsLiked = E1.StuID
    left join Course as C1 on C1.CID = E1.CID
where C1.CName like 'algorithm%'
group by S1.StuID
having count(distinct L1.WhoIsLiked) = 
    (
        select count(distinct E1.StuID)
        from Enrolled_in as E1
        left join Course as C1 on E1.CID = C1.CID
        where C1.CName like 'algorithm%'
    );

#4
select C1.CName, S1.Major as S1_Major, E1.Grade as S1_Grade, concat(S1.Fname, " ",S1.LName) as S1_name,
                S2.Major as S2_Major, E2.Grade as S2_Grade, concat(S2.Fname, " ",S2.LName) as S2_name
from Loves as Lv1
    left join Enrolled_in as E1 on Lv1.WhoLoves = E1.StuID
    left join Student as S1 on S1.StuID = Lv1.WhoLoves
    left join Loves as Lv2 on Lv1.WhoLoves = Lv2.WhoIsLoved and Lv1.WhoIsLoved = Lv2.WhoLoves
    left join Enrolled_in as E2 on Lv2.WhoLoves = E2.StuID
    left join Student as S2 on S2.StuID = Lv2.WhoLoves
    left join Course as C1 on C1.CID = E1.CID
where E1.CID = E2.CID;

#5
select S1.LName, count(S1.LName)
from Student as S1
where S1.Sex = 'F'
group by S1.LName
having count(S1.LName) = 
    (
        select max(temp.total)
        from
            (
                select count(S2.LName) as total
                from Student as S2
                where S2.Sex = 'F'
                group by S2.LName
            ) as temp
    );

#6
select S1.FName, S1.Lname, S2.FName, S2.Lname, S3.FName, S3.Lname 
from Loves as Lv1
    left join (select StuID, FName, Lname from Student) as S1 on Lv1.WhoLoves = S1.StuID
    join Loves as Lv2 on Lv1.WhoIsLoved = Lv2.WhoLoves
    left join (select StuID, FName, Lname from Student) as S2 on Lv2.WhoLoves = S2.StuID
    join Loves as Lv3 on Lv2.WhoIsLoved = Lv3.WhoLoves
    left join (select StuID, FName, Lname from Student) as S3 on Lv3.WhoLoves = S3.StuID
where Lv2.WhoIsLoved = Lv3.WhoLoves and Lv3.WhoIsLoved = Lv1.WhoLoves;

#7
select S1.Fname, S1.LName, S1.Age
from Student as S1
    join
        (
            select temp.Major, temp.Age as minAge
            from
                (
                    select *
                    from Student
                    order by Major asc, Age asc
                ) as temp
            group by Major
        ) as temp2 on S1.Major = temp2.Major
where S1.Age = temp2.minAge;

#8
select S1.Fname, count(S1.Fname)
from Student as S1
where S1.Sex = 'F'
group by S1.Fname
having count(S1.Fname) = 
    (
        select max(temp.total)
        from
            (
                select count(S2.Fname) as total
                from Student as S2
                where S2.Sex = 'F'
                group by S2.Fname
            ) as temp
    );

#9
select distinct D1.DName, F1.Fname, F1.Lname
from Student as S1
    left join Enrolled_in as E1 on S1.StuID = E1.StuID
    left join
        (
            select E1.CID ,avg(S1.Age) as AvgAge
            from Enrolled_in as E1
                left join Student as S1 on E1.StuID = S1.StuID
            group by E1.CID
        ) as temp1 on E1.CID = temp1.CID
    left join Faculty as F1 on F1.FacID = S1.Advisor
    left join Department as D1 on D1.DNO = S1.Major
where S1.Age < AvgAge;

#10
select D2.DName
from Student as S1
    left join Department as D1 on S1.Major = D1.DNO
    inner join Minor_in as M1 on M1.StuID = S1.StuID
    left join Department as D2 on M1.DNO = D2.DNO
    where D1.DName like 'math%' or D1.DName like 'computer%'
    group by M1.DNO
    having count(M1.DNO) = 
        (
            select max(temp.total)
            from
                (
                    select M1.DNO, count(M1.DNO) as total
                    from Student as S1
                        left join Department as D1 on S1.Major = D1.DNO
                        inner join Minor_in as M1 on M1.StuID = S1.StuID
                    where D1.DName like 'math%' or D1.DName like 'computer%'
                    group by M1.DNO
                ) as temp
        );

#11
select distinct * 
from Preferences as P1
    left join (select StuID, LName, Fname from Student) as S1 on P1.StuID = S1.StuID
    left join Preferences as P2 on P1.StuID != P2.StuID
    left join (select StuID, LName, Fname from Student) as S2 on P2.StuID = S2.StuID
where P1.SleepHabits != P2.SleepHabits
    or P1.MusicType != P2.MusicType
    or P1.Smoking != P2.Smoking;

#12
select distinct temp1.StuID, temp1.Fname, temp1.LName, temp1.Major, temp1.GPA, temp2.CID, C1.CName, temp2.Grade
from
    (
        select S1.*, (sum(gradepoint * Credits)/sum(Credits)) as GPA
        from Student as S1
            left join Enrolled_in as E1 on S1.StuID = E1.StuID
            left join Course as C1 on C1.CID = E1.CID
            left join Department as D1 on S1.Major = D1.DNO
            left join Gradeconversion as G1 on E1.Grade = G1.lettergrade
        group by S1.StuID
    ) as temp1
    inner join
    (
        select *
        from Enrolled_in
        where Grade = 'A' or Grade = 'A+'
    ) as temp2 on temp1.StuID = temp2.StuID 
    left join    Course as C1 on C1.CID = temp2.CID
where temp1.GPA >=
    (
        select gradepoint
        from Gradeconversion
        where lettergrade = 'B+'
    );

#13
select S1.StuID, S1.Fname, S1.LName, C1.CID, C1.CName
from Loves as Lv1
    left join Enrolled_in as E1 on E1.StuID = Lv1.WhoIsLoved
    left join Course as C1 on E1.CID = C1.CID
    left join Student as S1 on S1.StuID = Lv1.WhoLoves;

#14
select temp1.Fname, temp1.LName, temp2.LikesNum, count(distinct Lv2.WhoLikes) as LikedNum, temp2.LikesNum - count(distinct Lv2.WhoLikes) as Difference
from Likes as Lv1
    left join
        (
            select S1.StuID, S1.Fname, S1.LName
            from Student as S1
        ) as temp1 on temp1.StuID = Lv1.WhoLikes
    left join
        (
            select Lv2.WhoLikes as ID, count(Lv2.WhoIsLiked) as LikesNum
            from Likes as Lv2
            group by Lv2.WhoLikes
        ) as temp2 on temp2.ID = Lv1.WhoLikes
    left join Likes as Lv2 on Lv2.WhoIsLiked = Lv1.WhoLikes
where Lv1.WhoLikes = Lv2.WhoIsLiked
group by Lv2.WhoIsLiked;

#15
select *
from Lives_in as LI
    left join Lives_in as LI2 on LI.dormid = LI2.dormid and LI.room_number = LI2.room_number
    left join Student as S1 on S1.StuID = LI.stuid
group by LI.room_number
having count(LI.room_number) = 1;

#16
select *
from Student as S1
where S1.StuID not in
    (
        select S2.StuID
        from Student as S2
        inner join Lives_in as LI1 on S2.StuID = LI1.stuid
    );

#18
select S1.LName, S1.Fname, S1.Age, S1.Sex
from Student as S1
    left join Enrolled_in as E1 on E1.StuID = S1.StuID
where E1.CID = '600.315'
    or E1.CID = '600.415'
    or E1.CID like '600.1%'
    or E1.CID like '600.2%';

#19
select concat(F1.Fname, ' ', F1.Lname) as F_name, count(E1.StuID) as total_enrollment
from Course as C1
    left join Faculty as F1 on C1.Instructor = F1.FacID
    left join Member_of as M1 on M1.FacID = F1.FacID
    left join Department as D1 on D1.DNO = C1.DNO
    inner join Enrolled_in as E1 on E1.CID = C1.CID
where D1.DName like 'computer%' and M1.Appt_Type like 'primary%'
group by F1.FacID
order by total_enrollment desc;

#20
select *
from Student as S1
    left join Enrolled_in as E1 on E1.StuID = S1.StuID
    left join Course as C1 on C1.CID = E1.CID
    left join
        (
            select E1.StuID, sum(C1.Credits) total_credits
            from Enrolled_in as E1
                left join Course as C1 on E1.CID = C1.CID
            group by E1.StuID
        ) as temp1 on temp1.StuID = S1.StuID
where C1.CName like '%database%' and
    temp1.total_credits > 
        (
            select avg(C1.Credits)
            from Enrolled_in as E1
                inner join Course as C1 on C1.CID = E1.CID
        );

#21
select *
from Student as S1
where S1.Age >
    (
        select std(Age)+avg(Age)
        from Student as S1
        left join Enrolled_in as E1 on S1.StuID = E1.StuID
        left join Course as C1 on C1.CID = E1.CID
        where C1.CName like '%database%'
    );

#22
select concat(S1.Fname, ' ', S1.LName) as Name, S1.Age
from Student as S1, Minor_in as M1, Department as D1, Member_of as Me1, Faculty as F1
where
    S1.StuID = M1.StuID and
    M1.DNO = D1.DNO and
    D1.DNO = Me1.DNO and
    F1.FacID = Me1.FacID and
    F1.Sex = 'F' and
    Me1.Appt_Type like '%primary%' and
    D1.Division = 'EN';

#23
select S1.StuID, concat(S1.Fname, ' ', S1.LName) as Name
from Student as S1, Enrolled_in as E1, Course as C1, Faculty as F1
where
    S1.StuID = E1.StuID and
    E1.CID = C1.CID and
    C1.Instructor = F1.FacID and
    F1.Fname = 'Paul' and
    F1.Lname = 'Smolensky';

#24
select concat(S1.Fname, ' ', S1.LName) as Name, S1.StuID
from Student as S1, Enrolled_in as E1, Enrolled_in as E2, Enrolled_in as E3, Student as S2
where
    S1.StuID = E1.StuID and
    E1.CID = E2.CID and
    E1.StuID != E2.StuID and
    E2.CID = E3.CID and
    E2.StuID != E3.StuID and
    E3.StuID = S2.StuID and
    E1.StuID != E3.StuID and
    S2.Fname = 'Linda' and
    S2.LName = 'Smith';

#25
select E1.CID, count(E1.Grade) as Above_A_minus, temp1.total_enroll, (count(E1.Grade)/temp1.total_enroll*100) as percentage
from Enrolled_in as E1
    left join
        (
            select E2.CID, count(E2.StuID) as total_enroll
            from Enrolled_in as E2
            group by E2.CID
        ) as temp1 on temp1.CID = E1.CID
where
    E1.Grade in ('A', 'A-', 'A+') and
    temp1.total_enroll > 0
group by E1.CID;

#26
select temp1.CName, temp1.CID, concat(F1.Fname, ' ', F1.Lname) as Name
from
    (
        select F1.FacID, C1.CID, C1.CName, (count(E1.Grade)/temp1.total_enroll*100) as percentage
        from Enrolled_in as E1
            left join
                (
                    select E2.CID, count(E2.StuID) as total_enroll
                    from Enrolled_in as E2
                    group by E2.CID
                ) as temp1 on temp1.CID = E1.CID
            left join Gradeconversion as G1 on E1.Grade = G1.lettergrade
            left join Course as C1 on C1.CID = E1.CID
            left join Faculty as F1 on F1.FacID = C1.Instructor
        where
            G1.gradepoint < 2.7 and
            temp1.total_enroll > 0
        group by E1.CID
    ) as temp1
    left join Faculty as F1 on F1.FacID = temp1.FacID
where temp1.percentage = 
    (
        select max(temp2.percentage)
        from
            (
                select (count(E1.Grade)/temp1.total_enroll*100) as percentage
                from Enrolled_in as E1
                    left join
                        (
                            select E2.CID, count(E2.StuID) as total_enroll
                            from Enrolled_in as E2
                            group by E2.CID
                        ) as temp1 on temp1.CID = E1.CID
                    left join Gradeconversion as G1 on E1.Grade = G1.lettergrade
                    left join Course as C1 on C1.CID = E1.CID
                    left join Faculty as F1 on F1.FacID = C1.Instructor
                where
                    G1.gradepoint < 2.7 and
                    temp1.total_enroll > 0
                group by E1.CID
            ) as temp2
    );

#30-a
select distinct D1.city2_code as city1, D2.city2_code as city2, D1.distance + D2.distance as distance_through_BAL
from Direct_distance as D1, Direct_distance as D2
where
    D1.city1_code = 'BAL' and
    D1.city1_code = D2.city1_code and D1.city2_code != D2.city2_code and D1.city1_code != D2.city2_code;

#30-b
select distinct C1.city_code as city1, C2.city_code as city2, 
    sqrt(power((70*(C1.latitude - C2.latitude)),2)+power((70*(C1.longitude - C2.longitude)),2)) as rec_distance
from City as C1, City as C2
where
    C1.city_code != C2.city_code;
    
#30-c
select distinct temp1.city1, temp1.city2, D1.distance, temp1.rec_distance, temp2.distance_through_BAL
from 
    (
        select distinct C1.city_code as city1, C2.city_code as city2, 
            sqrt(power((70*(C1.latitude - C2.latitude)),2)+power((70*(C1.longitude - C2.longitude)),2)) as rec_distance
        from City as C1, City as C2
        where
            C1.city_code != C2.city_code
    ) as temp1
    left join
    (
        select distinct D1.city2_code as city1, D2.city2_code as city2, D1.distance + D2.distance as distance_through_BAL
        from Direct_distance as D1, Direct_distance as D2
        where
            D1.city1_code = 'BAL' and
            D1.city1_code = D2.city1_code and D1.city2_code != D2.city2_code and D1.city1_code != D2.city2_code
    ) as temp2 on
        temp1.city1 = temp2.city1 and temp1.city2 = temp2.city2 or
        temp1.city2 = temp2.city1 and temp1.city1 = temp2.city2
    left join Direct_distance as D1 on
        D1.city1_code = temp1.city1 and D1.city2_code = temp1.city2 or
        D1.city2_code = temp1.city1 and D1.city1_code = temp1.city2;

#30-d
select *
from
(
    (
        select distinct D1.city2_code as city1, D2.city2_code as city2, D1.distance + D2.distance as distance
        from Direct_distance as D1, Direct_distance as D2
        where
            D1.city1_code = 'BAL' and
            D1.city1_code = D2.city1_code and D1.city2_code != D2.city2_code and D1.city1_code != D2.city2_code
    )
    union
    (
        select distinct C1.city_code as city1, C2.city_code as city2, 
            sqrt(power((70*(C1.latitude - C2.latitude)),2)+power((70*(C1.longitude - C2.longitude)),2)) as distance
        from City as C1, City as C2
        where
            C1.city_code != C2.city_code
    )
    union
    (
        select distinct D1.city1_code as city1, D1.city2_code as city2, D1.distance
        from Direct_distance as D1
    )
) as temp
group by temp.city1, temp.city2
order by temp.distance asc;

#31
select LI1.dormid, avg(DA1.distance) as avgDistance
from Lives_in as LI1, Student as S1, Direct_distance as DA1
where
    LI1.stuid = S1.StuID and
    DA1.city2_code = S1.city_code and
    DA1.city1_code = 'BAL'
group by LI1.dormid
having avg(DA1.distance) >= 
    (
        select max(temp1.avgDistance)
        from
            (
                select LI1.dormid, avg(DA1.distance) as avgDistance
                from Lives_in as LI1, Student as S1, Direct_distance as DA1
                where
                    LI1.stuid = S1.StuID and
                    DA1.city2_code = S1.city_code and
                    DA1.city1_code = 'BAL'
                group by LI1.dormid
            ) as temp1
    );

#32
select concat(S1.Fname, ' ', S1.LName) as Name, S1.Age, S1.Major
from
    (
        select S1.StuID, avg(G1.gradepoint) as major_GPA
        from Student as S1, Enrolled_in as E1, Gradeconversion as G1
        where
            S1.StuID = E1.StuID and
            E1.CID like concat(S1.Major,"%") and
            G1.lettergrade = E1.Grade
        group by S1.StuID
    ) as temp1,
    (
        select S1.StuID, avg(G1.gradepoint) as non_major_GPA
        from Student as S1, Enrolled_in as E1, Gradeconversion as G1
        where
            S1.StuID = E1.StuID and
            E1.CID not like concat(S1.Major,"%") and
            G1.lettergrade = E1.Grade
        group by S1.StuID
    ) as temp2,
    Student as S1
where
    temp1.StuID = temp2.StuID and
    temp2.non_major_GPA > temp1.major_GPA and
    S1.StuID = temp1.StuID;

#33
select D1.DName, count(distinct S1.StuID)/count(distinct M1.FacID) as mean_number_of_advisee
from Member_of as M1, Department as D1, Student as S1
where
    D1.DNO = M1.DNO and
    M1.Appt_Type like '%primary%' and
    S1.Advisor = M1.FacID
group by D1.DNO;

#34
select A1.activity_name, count(distinct P1.stuid) as student_members
from Activity as A1, Participates_in as P1, Faculty_Participates_in as F1
where
    A1.actid = P1.actid and
    A1.actid = F1.actid
group by A1.actid
having count(distinct P1.stuid)+count(distinct F1.FacID) >=
    (
        select max(temp1.total_members)
        from
            (
                select count(distinct P1.stuid)+count(distinct F1.FacID) as total_members
                from Activity as A1, Participates_in as P1, Faculty_Participates_in as F1
                where
                    A1.actid = P1.actid and
                    A1.actid = F1.actid
                group by A1.actid
            ) as temp1
    );

#35
select temp1.activity_name
from
    (
        select A1.*
        from Activity as A1
            left join Participates_in as P1 on A1.actid = P1.actid
        group by A1.actid
        having count(P1.stuid) = 0
    ) as temp1
    left join Faculty_Participates_in as F1 on temp1.actid = F1.actid
group by temp1.actid
having count(F1.FacID) > 0;

#36
select distinct concat(S1.Fname, ' ', S1.LName) as Name
from Enrolled_in as E1, Student as S1, Enrolled_in as E2, Lives_in as LI1, Lives_in as LI2, Student as S2, City as C1
where
    E1.StuID = S1.StuID and
    E1.CID = E2.CID and
    E1.StuID != E2.StuID and
     E2.StuID = LI1.stuid and
    LI1.dormid = LI2.dormid and
    LI1.room_number = LI2.room_number and
    LI1.stuid != LI2.stuid and
    LI2.stuid = S2.StuID and
    S2.city_code = C1.city_code and
    C1.state = 'PA';
    
#37
select distinct concat(S1.Fname,' ', S1.LName) as Name1, concat(S2.Fname,' ', S2.LName) as Name2
from Lives_in as LI1, Lives_in as LI2, Student as S1, Student as S2, City as C1, City as C2
where
    LI1.dormid = LI2.dormid and
    LI1.room_number = LI2.room_number and
    LI1.stuid < LI2.stuid and ##!!important: prevent duplicated pairs
    S1.StuID = LI1.stuid and
    S2.StuID = LI2.stuid and
    S1.city_code = C1.city_code and
    S2.city_code = C2.city_code and
    C1.country != C2.country;

#38
select S1.Major, count(P1.actid)/count(distinct S1.StuID) as average_activities
from Student as S1, Participates_in as P1
where
    S1.StuID = P1.stuid
group by Major;

#39
select distinct concat(F1.Fname, ' ', F1.LName) as Faculty_Name
from Student as S1
    left join Member_of as M1 on S1.Advisor = M1.FacID
    left join Enrolled_in as E1 on E1.StuID = S1.StuID
    left join Enrolled_in as E2 on E1.CID = E2.CID and E1.StuID != E2.StuID
    left join Lives_in as LI1 on LI1.stuid = E2.StuID
    left join Lives_in as LI2 on LI1.dormid = LI2.dormid and LI1.room_number = LI2.room_number and LI1.stuid != LI2.stuid
    left join Student as S2 on LI2.stuid = S2.StuID
    left join Minor_in as MI1 on S2.StuID = MI1.StuID
    left join Faculty as F1 on F1.FacID = S1.Advisor 
where
    S2.Major = M1.DNO or
    MI1.DNO = M1.DNO;

#40
select concat(S1.Fname, ' ', S1.LName) as Name, S1.Major, sum(C1.Credits) as total_credit, temp1.average_credits
from Student as S1
    left join Enrolled_in as E1 on S1.StuID = E1.StuID
    left join Course as C1 on E1.CID = C1.CID
    left join
        (
            select S1.Major, sum(C1.Credits)/count(distinct S1.StuID) as average_credits
            from Student as S1
                left join Enrolled_in as E1 on S1.StuID = E1.StuID
                left join Course as C1 on E1.CID = C1.CID
            group by S1.Major
        ) as temp1 on S1.Major = temp1.Major
group by S1.StuID
having sum(C1.Credits) > temp1.average_credits;