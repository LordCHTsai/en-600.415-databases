(1)Team members
  Cheng-Hsuan Tsai (ctsai20@jhu.edu)
  Ji-Yuan Huang (hjiyuan1@jhu.edu)


(2)Target domain
  Crime incidents database from DC.gov


(3)List of abilities of the system
  1. The average crime incidents per month in a specific district.
  2. The ratio of crime incidents occurred at night to the total crime incidents in a day.
  3. Show the crime incidents in the google map depends on the geographic location.
  4. Notify a user if a crime accident occurred in 5 miles from his/her house.
  5. Display a structure diagram shows the percentage of each type of offense.
  6. Calculate the total number of criminals using gun, grouped by offense type.
  7. Show sexual abuse occurs in some specific shifts and locations.
  8. Inform some specific users when the offense method is Gun which occurs more than 3 times in one day.
  9. Show the Robbery that occurs with gun if it is in some specific locations.
  10. Show the location of the case of sexual abuse that increase the most in these years
  11. Show the percentage of every offense in a circle with 5 miles radius when the user gives the coordinates.
  12. Show the percentage of all kinds of offense group by shift.
  13. Show the female user who lives in or next to the most dangerous location.
  14. Show the type of offense which is decreased between 2011 and 2012 but increased between 2012 and 2013.
  15. Show the district that sexual abuse occurs the most where is occupied by the most male users.


(4)Relational data model
  CREATE TABLE CrimeRecord {
    CCN               INTEGER         NOT NULL,
    ReportDate        DATE            NOT NULL,
    Shift             TIME            NULL,
    LastModifiedDate  DATE            NOT NULL,
    PRIMARY KEY (CCN)
  };

  CREATE TABLE CrimeContent {
    CCN               INTEGER         NOT NULL,
    Offense           VARCHAR(255)    NOT NULL,
    Method            VARCHAR(255)    NOT NULL,
    FOREIGN KEY (CCN)
      REFERENCES CrimeRecord (CCN)
      ON DELETE CASCADE
      ON UPDATE CASCADE
  };

   CREATE TABLE CrimeLocation {
    CCN               INTEGER         NOT NULL,
    Address           VARCHAR(255)    NOT NULL,
    District          INTEGER         NOT NULL,
    Xcoord            INTEGER         NULL,
    Ycoord            INTEGER         NULL,
    FOREIGN KEY (CCN)
      REFERENCES CrimeRecord (CCN)
      ON DELETE CASCADE
      ON UPDATE CASCADE
  };

  CREATE TABLE Users {
    UID               INTEGER         NOT NULL,
    Fname             VARCHAR(20)     NOT NULL,
    Lname             VARCHAR(20)     NOT NULL,
    Sex               VARCHAR(5)      NULL,
    Address           VARCHAR(255)    NOT NULL,
    Xcoord            INTEGER         NULL,
    Ycoord            INTEGER         NULL,
    PRIMARY KEY (UID)
  };

   CREATE TABLE UsersPassword {
    UID               INTEGER         NOT NULL,
    Salt              VARCHAR(32)     NOT NULL,
    HashedPwd         VARCHAR(256)    NOT NULL,
    FOREIGN KEY (UID)
      REFERENCES Users (UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
  };

(5)SQL statement

(6)How will the data be loaded
  There are two steps to load the data.
    1. Download the raw crime incidents record from http://data.dc.gov/ and convert it to sql format with the tables we listed in part (4).
    2. Using data extraction from some websites to grab some useful information such as the patrol route or the addresses of the police stations.


(7)Views of the system
  Basically, the web page will beginning with a login page, which you can sign in or log in our system. After log in the system,
  the page will contain a large block of the Google map, then a search bar will in the top of the page that users can choose what they
  want to show on the Google map. For example, they can input an address and choose the radius to show the incidents, or they can use
  their geographic location by just click a button (HTML 5 now support the feature to get the geographic location of the computer). There
  will also exists some features that we listed in part (3), the statistics data will be presented using dynamic diagram (eg. D3.js).


(8)Topics
  - Security
    1. The whole system will be built as a webapp and the communication between client and server will be under the Secure Sockets Layer (SSL).
    2. A member registration system will be implemented with all password hashed with salt to prevent the attackers steal the password.
    3. All queries send to the database will be parsed to check if there exists malicous code (eg. SQL injection, Javascript, etc.).
    4. The database will restrict the ability of each user to access the tables.

  - Optimizaion
    1. In order to update the information on the webpage in time, usually the page will send http requests to the server in every few seconds.
       Since each http request will build a connection to the server then disconnect it after the server replys the request, this will increases
       the workload of the server when many users browse the same page. Thus, to improve it, we can build a connection between front-end and back-end
       when the user stay in the same page and only disconnect it when the user leave the page.

  - Data extraciton
    1. Since we only have the records of crime incidents, we can't extend the service of the system such as: finding the nearest police station or the
       nearest hospital. Thus, we can further parse some websites on the internet to find some useful information.

  - Advanced GUI
    1. The front-end will be implemented using Angularjs framework which provides a MVC (Models, Views, and Controllers) structure to make the
       information on the web page can be updated without refresh the whole page (like Ajax).
    2. Google map API will be used in our system, with markers user can easily seeing where the crime incidents occurred.
    3. There will be lots of diagram to show the statistics result.


(9)Platform
  The system will be run on a dedicated server located at Los Angeles, California. Following is the spec of the server:
    - Intel Dual Xeon 5148 (4*2.33 GHz)
    - 8 GB RAM
    - Storage 2*500 GB HHD
  OS: CentOS 6
  Web server: Nginx 1.4.4
  Database: MySQL 5.7
