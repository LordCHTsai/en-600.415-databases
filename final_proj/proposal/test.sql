The average crime incidents per month in a specific district

SELECT AVG(TEMP.Total)
FROM  (
    SELECT COUNT( CCN ) AS Total
    FROM CrimeRecord AS CR
      LEFT JOIN CrimeLocation AS CL ON CL.CCN = CR.CCN
    WHERE
      CL.District = [SPECIFIC DISTRICT]
    GROUP BY
      EXTRACT(MONTH FROM iCR.ReportDate)
    ) AS TEMP;

Notify a user if a crime accident occurred in a specific distance from his/her house

SELECT U.UID, U.Fname, U.Lname
FROM CrimeRecord as CR, CrimeLocation AS CL, Users AS U
WHERE CR.CCN = CL.CCN
HAVING SQRT(POWER(CL.Xcoord-U.Xcoord)+POWER(CL.Ycoord-U.Ycoord)) < [SPECIFIC DISTANCE]


